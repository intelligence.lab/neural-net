/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package neuralnetowrk;


public class Neuron {
    public double localInput;
    public double localOutput;
    public double Weight[];
    public double biasVal;
    public double gradient;
    //these are variable entities,so we always get the current one from previous
    public double currDeltaWeight[];
    //the weights that were current until we just changed them.
    public double prevDeltaWeight[];
    public double deltaBias;
    public double prevDeltaBias;
    
    

    /**
     * 
     * @param numOfInputEdges numOfInputEdges is the number of neurons from previous layer
     * which will take part in generating output.
     */
    public Neuron(int numOfInputEdges)
    {
        Weight=new double[numOfInputEdges];
        currDeltaWeight=new double[numOfInputEdges];
        prevDeltaWeight=new double[numOfInputEdges];
        for(int i=0;i<numOfInputEdges;i++)
            Weight[i]=-1+2*Math.random(); //random values in between -1 to 1

        biasVal=-1+2*Math.random();



    }

    public double getOutput()
    {
        return localOutput;
    }

    public void setInput(double input)
    {
         localInput=input; 
    }




}
