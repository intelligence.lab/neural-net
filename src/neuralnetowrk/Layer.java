/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package neuralnetowrk;

public class Layer {

    //the array of neurons in this layer
    public Neuron[] neurons;
    //keep track of all the incomings from other layer
    public double input[];
    //the list of values we will pass to other layer as input
    public double output[];

    public void Forward(double[] inputVector)
    {
        Neuron neuron;
        double sum=0;
        input=inputVector;
        for(int i=0; i<neurons.length; i++)
        {
            neuron=neurons[i];

            for(int j=0;j<neuron.Weight.length;j++)
            {
                sum+=input[j]*neuron.Weight[j];
            }

            sum+=neuron.biasVal;

            neuron.localInput=sum;
            neuron.localOutput=Sigmoid(sum);

            output[i]=neuron.localOutput;      
        }
        
    }

    public double Sigmoid (double sum) {
        if (sum < -45.0) return 0.0;
        else if (sum > 45.0) return 1.0;
		return 1/(1+Math.exp(-sum));
	}
    /**
     * 
     * @param numOfNeuron Neurons in this layer
     * @param numOfNeuronFromPrevLayer Neurons in previous layer
     */
    public Layer(int numOfNeuron, int numOfNeuronFromPrevLayer)
    {
        neurons=new Neuron[numOfNeuron];
        output=new double[numOfNeuron];
        input=new double[numOfNeuronFromPrevLayer];

        for(int i=0;i<numOfNeuron;i++)
            neurons[i]=new Neuron(numOfNeuronFromPrevLayer); //there will be numOfNeuronFromPrevLayer edges from previous layer
        //to this neuron

    }

    public double[] getOutputVector()
    {
        return output;
    }
    public void setInputVector(double inputVector[])
    {
        input=inputVector;

    }
    public void setOutputVector(double outputVector[])
    {
        output=outputVector;

    }

}
