/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package neuralnetowrk;

import java.util.ArrayList;

public class BackPropagation {

    public int numOfLayer;
    public int numOfClass=3; // the possible output are 3 ways
    double eta = 0.01;  // learning rate - controls the maginitude of the increase in the change in weights. found by trial and error.
    double alpha = 0.7; // momentum - to discourage oscillation. found by trial and error.

    public Layer[] layers;

    public double[] actualOutput;
    public double[] estimatedOutput;

    public void  updateGradientValueOfOutputNeurons(double[] actual,Layer layer)
    {
        Neuron nr;
        double x;

        for(int i=0;i<layer.neurons.length;i++)
        {
            nr=layer.neurons[i];
            x=nr.localOutput;
            nr.gradient=(actual[i]-x)*x*(1-x);

        }


        
    }

    /**
     * 
     * @param actual
     * @param layer the layer we will be checking
     * @param upperLayer the layer from which this layer gets input
     */
    public void  updateGradientValueOfHiddenLayerNeurons(double[] actual,Layer layer,Layer upperLayer)
    {
        Neuron nr;
        double output;
        double sum;


        for(int i=0;i<layer.neurons.length;i++)
        {
            sum=0;
            nr=layer.neurons[i];
            output=nr.localOutput;
            for(int j=0;j<upperLayer.neurons.length;j++)
                sum+=upperLayer.neurons[j].gradient*upperLayer.neurons[j].Weight[i];
            

           nr.gradient=output*(1-output)*sum;

        }



    }

    public void updateDeltaWeight(Layer layer)
    {
        Neuron nr;
        for(int i=0;i<layer.neurons.length;i++)
        {
            nr=layer.neurons[i];


            for(int j=0;j<nr.currDeltaWeight.length;j++)
            {
                nr.prevDeltaWeight[j]=nr.currDeltaWeight[j];
                nr.currDeltaWeight[j]=eta * layer.input[j] * nr.gradient;
            }

        }
        
    }
    public void updateDeltaBias(Layer layer)
    {
        Neuron nr;
        for(int i=0;i<layer.neurons.length;i++)
        {
            nr=layer.neurons[i];
            nr.prevDeltaBias=nr.deltaBias;
           nr.deltaBias=eta*nr.gradient;

        }

    }

    public void updateWeights(double[] actual,Layer[] layers)
    {
        updateGradientValueOfOutputNeurons( actual,layers[layers.length-1]);

        for(int i=layers.length-2;i>0;i--)
            updateGradientValueOfHiddenLayerNeurons(actual,layers[i],layers[i+1]);
        
        for(int i=1;i<layers.length;i++)
        {
          updateDeltaWeight(layers[i]);
          updateDeltaBias(layers[i]);
        }
        Layer lr;
        Neuron nr;
        //i=1;skipping first layer
        for(int i=1;i<layers.length;i++)
        {
            lr=layers[i];
            for(int j=0;j<lr.neurons.length;j++)
            {
                nr=lr.neurons[j];

                for(int k=0;k<nr.Weight.length;k++)
                {
                    nr.Weight[k]+=nr.currDeltaWeight[k];
                    nr.Weight[k]+=alpha*nr.prevDeltaWeight[k];



                }
                nr.biasVal+=nr.deltaBias;
                nr.biasVal+=alpha*nr.prevDeltaBias;
               
                
            }
          
        }




        
    }


   


    public BackPropagation(Network NN, ArrayList<Record> records, int runs)
    {
        estimatedOutput=new double[numOfClass];
        
        numOfLayer=NN.numOfLayer;
        layers=NN.layers;
        Record record;
        int classOfRecord;
    
        double[] input=new double[4];
        double[] actual=new double[3];
        for(int i=0;i<runs;i++)
        {
            for(int j=0;j<records.size();j++)
            {
                for(int k=0;k<3;k++)
                    actual[k]=0;
                record=records.get(j);
                input=record.getRecord();
                classOfRecord=(int)record.Cls;

                layers[0].setInputVector(input);
                layers[0].setOutputVector(input);

                layers[1].Forward(layers[0].output);
                layers[2].Forward(layers[1].output);

                //set 1 for the class of the given record
                actual[classOfRecord-1]=1;

                updateWeights(actual, layers);
            }
        }

    }

    

}
