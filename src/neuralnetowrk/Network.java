/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package neuralnetowrk;

public class Network {
    Layer[] layers;
    int numOfLayer;
    /**
     * 
     * @param num total numbers of layers in network.
     */
    public Network(int num)
    {
         numOfLayer=num;
         layers=new Layer[numOfLayer];
         layers[0]=new Layer(3,3); // will take 3 inputs, has 3 neurons
         layers[1]=new Layer(4,3); // will take 3 inputs, has 4 neurons
         layers[2]=new Layer(3,4); // will take 4 inputs, has 3 neurons; final one; so there will be 3 final output
    }

}
