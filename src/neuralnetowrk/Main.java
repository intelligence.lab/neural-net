/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package neuralnetowrk;

import java.util.ArrayList;

public class Main {
    public static final int HIDDEN_LAYERS = 1;//number of hidden layers
    public static final int RUNS = 1000;

    
    public static void main(String[] args) {
        // TODO code application logic here


     ArrayList<Record> trainingRecords = new ArrayList<Record>();
     ArrayList<Record> testingRecords = new ArrayList<Record>();

     
     trainingRecords=ReadFile.buildRecords("data/Train.txt");
     testingRecords=ReadFile.buildRecords("data/Test.txt");

     System.out.println("\nSize of training records:"+trainingRecords.size());

        Network NN=new Network(2+HIDDEN_LAYERS);//2 layers are always fixed;input & output

        BackPropagation bp=new BackPropagation(NN, trainingRecords,RUNS);
        double[] input=new double[3];
        int mismatched=0;
        int Cls,estimatedCls;
        System.out.println("\n\nStarting to process "+testingRecords.size()+" test records...\n\n");
        for(int i=0;i<testingRecords.size();i++)
        {
          input=testingRecords.get(i).getRecord();
          Cls=(int)testingRecords.get(i).Cls;

          NN.layers[0].setInputVector(input);
          NN.layers[0].setOutputVector(input);

          NN.layers[1].Forward(NN.layers[0].output);
          NN.layers[2].Forward(NN.layers[1].output);

          if(NN.layers[2].output[0]>NN.layers[2].output[1])
          {
           if(NN.layers[2].output[0]>NN.layers[2].output[2])
           {
             estimatedCls=1;
           } else estimatedCls=3;
         }

         else
         {
           if(NN.layers[2].output[1]>NN.layers[2].output[2])
           {
             estimatedCls=2;
           } else estimatedCls=3;
         }

         if(Cls!=estimatedCls)
         {
          mismatched++;
        }
        
      }
      System.out.println("Num of Data Missmatched:"+mismatched);


      
    }

  }
